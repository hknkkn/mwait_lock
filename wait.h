#include <stdlib.h>

#ifdef DEBUG
#define debugf(fmt, ...) do { printf(fmt, ##__VA_ARGS__); fflush(stdout);} while(0)
#else
#define debugf(fmt, ...)
#endif

#define errorf(fmt, ...) do { printf("\033[0;31m" fmt "\033[0m", ##__VA_ARGS__); fflush(stdout);} while(0)
#define stallf(var, fmt, ...) do { errorf(fmt, ##__VA_ARGS__); while(var);} while(0)

typedef char cacheline_t[CACHELINESIZE / sizeof(char)] __attribute__((aligned(64)));

typedef int monitor_t;

/* Following 2 functions are from Linux kernel */
static inline void __monitor(const void *eax, unsigned long ecx,
				 unsigned long edx)
{
	/* "monitor %eax, %ecx, %edx;" */
	asm volatile(".byte 0x0f, 0x01, 0xc8;"
			 :: "a" (eax), "c" (ecx), "d"(edx));
}
static inline void __mwait(unsigned long eax, unsigned long ecx)
{
	/* "mwait %eax, %ecx;" */
	asm volatile(".byte 0x0f, 0x01, 0xc9;"
			 :: "a" (eax), "c" (ecx));
}

static inline long sync_mwait_neq(volatile monitor_t *valaddr, monitor_t notifyval)
{
	long spun = 0;
	do {
		__monitor((void*)valaddr,0,0);
		if (*valaddr != notifyval)
			break;
		__mwait(0,0);
	} while (*valaddr == notifyval && ++spun);
	return spun;
}

static inline long sync_mwait_eq(volatile monitor_t *valaddr, monitor_t notifyval)
{
	long spun = 0;
	do {
		__monitor((void*)valaddr,0,0);
		if (*valaddr == notifyval)
			break;
		__mwait(0,0);
	} while (*valaddr != notifyval && ++spun);
	return spun;
}

/* From libgomp */
#define cpu_relax()	do { asm volatile ("nop;" : : : "memory"); } while(0)
//#define cpu_relax() do {} while(0)

static int do_mwait = -1;
static inline int check_mwait() {
	if (do_mwait == -1) {
		if (getenv("smwait") && getenv("smwait")[0] == '1')
			do_mwait = 1;
		else
			do_mwait = 0;
	}
	return do_mwait;
}

static inline long sync_wait_eq(volatile monitor_t *region, monitor_t notifyval)
{
	long ret = 0;

	if (check_mwait()) ret = sync_mwait_eq(region, notifyval);
	else while(*region != notifyval) { cpu_relax(); ret++;}

	return ret;
}

static inline long sync_wait_neq(volatile monitor_t *region, monitor_t notifyval)
{
	long ret = 0;

	if (check_mwait()) ret = sync_mwait_neq(region, notifyval);
	else while(*region == notifyval) { cpu_relax(); ret++;}

	return ret;
}

