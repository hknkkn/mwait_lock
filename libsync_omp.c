#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <linux/unistd.h>
#include <stdio.h>
#include <sched.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "libsync.h"
#include "cycle.h"
#include <assert.h>
#include <sys/ioctl.h>
#include "util.h"
#include "cmpxchg.h"
#include <omp.h>

/*
Very similar to pthread mutexes. Read the comment there.
*/
struct lock {
	omp_lock_t l;
};



lock_t* lock_init_shared(void)
{
	lock_t *m;
	assert(0 == posix_memalign((void**)&m, 64, sizeof(lock_t)));
	omp_init_lock(&m->l);
	return m;
}

int lock_set(lock_t *l, int rank)
{
	omp_set_lock(&l->l);
	return 0;
}

int lock_unset(lock_t *l, int rank)
{
	omp_unset_lock(&l->l);
	return 0;
}


