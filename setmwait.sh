#!/bin/bash

# This enables MONITOR/MWAIT from userspace.
# Works for AMD chips only. Requires rdmsr/wrmsr
# tools (msr-tools package). Does not make this
# change in the first 2 cpus for safety.
#
# usage: $0 [action]
# where action is
#   0 to disable
#   1 to enable
#  -1 to just show the current value of the MWAIT bit

msr_reg=0xC0010015
mwait_bit=10

mask=$((1 << $mwait_bit))

enbl=$1
[ -z "$enbl" ] && enbl=1

for i in `seq 0 $(($(grep processor /proc/cpuinfo | wc -l) - 1))`; do 

	oldval=$(sudo rdmsr -p $i -d $msr_reg)
	if [ $? != 0 ]; then
		echo rdmsr failed. is msr module loaded?
		exit
	fi
	if [ "$enbl" == "1" ]; then
		newval=$(( $oldval | $mask))
	elif [ "$enbl" == "-1" ]; then
		echo $i: $oldval \($(($oldval & $mask))\)
		continue
	else
		newval=$(( $oldval & ~($mask)))
	fi
#	echo $newval
	sudo wrmsr -p $i $msr_reg $newval
done
