#ifndef _SYNC_UTIL_H_
#define _SYNC_UTIL_H_


#define NBARR 1e5
#define NSPIN 1e7

#define WORKLOAD   a^=a+a; \
			a^=a+a+a; \
			a>>=b; \
			a>>=a+a; \
			a^=a<<b; \
			a^=a+b; \
			a+=(a+b)&07; \
			a^=n; \
			b^=a; \
			a|=b;

#define FIVE(A) A A A A A

#define TEN(A)  FIVE(A) FIVE(A)

#define FIFTY(A) TEN(A) TEN(A) TEN(A) TEN(A) TEN(A)

#define HUNDRED(A)  TEN(A) TEN(A) TEN(A) TEN(A) TEN(A) \
			TEN(A) TEN(A) TEN(A) TEN(A) TEN(A)

#define THOUSAND(A)  HUNDRED(A) HUNDRED(A) HUNDRED(A) HUNDRED(A) HUNDRED(A) \
			HUNDRED(A) HUNDRED(A) HUNDRED(A) HUNDRED(A) HUNDRED(A)

//static int __attribute__((optimize("O0"))) do_spin(unsigned long long iter, int n){
static int do_spin(unsigned long long iter, int n){
	int a = iter / 2;
	int b = n * 2;
	while(iter--) {
		TEN(WORKLOAD);
	}
	return a + b / n;
}


static void say_hi(int rank, int cpu)
{
	printf("Hi from thread %d on cpu %d\n", rank, sched_getcpu());
	fflush(stdout);
}

static void set_affinity(int cpu)
{
	cpu_set_t cpusetp;
	CPU_ZERO(&cpusetp);
	CPU_SET(cpu,&cpusetp);
	if(sched_setaffinity(0,sizeof(cpusetp),&cpusetp))
		perror("set_affinity");
}

#include <assert.h>
typedef unsigned long u64;
static void* get_page_addr(void* addr) {
	int fd = open("/proc/self/pagemap", O_RDONLY);
	int err = 0;
	void* ret;
//      unsigned long address = (unsigned long)addr & ~(PAGESIZE - 1);
	unsigned long vpfn = (unsigned long)addr >> 12;
	u64 flags;
	u64 pfn;
	assert(fd > 0);
	lseek(fd, (unsigned long)vpfn * 8, SEEK_SET);
	err = read(fd, &flags, 8);
	if (err < 0 || (err % 8) != 0)
		printf("read returned %d\n", err);
//	printf("flags=%lu \n", flags);
	close(fd);
	pfn = (unsigned long)flags & (((unsigned long)1 << 54) - 1);
	ret =  (void*)(0xffffea0000000000 + 64 * pfn);
	//printf("&page for %p: %p\n", addr, ret);
	return ret;
}

#endif
//#define DEBUG


