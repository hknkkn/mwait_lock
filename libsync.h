

typedef struct lock lock_t;

lock_t *lock_init_shared(void);
int lock_set(lock_t *lock, int rank);
int lock_unset(lock_t *lock, int rank);
