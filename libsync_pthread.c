#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <linux/unistd.h>
#include <stdio.h>
#include <sched.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "libsync.h"
#include "cycle.h"
#include <assert.h>
#include <sys/ioctl.h>
#include "util.h"
#include "cmpxchg.h"
#include <pthread.h>

/*
This directly uses pthread mutexes.
On 48 threads, some tasks end up taking the lock
only 24 times less than some others. Not fair at all!
Distribution is very sparse.
*/

struct lock {
	pthread_mutex_t m;
};



lock_t* lock_init_shared(void)
{
	lock_t *m;
	assert(0 == posix_memalign((void**)&m, 128, sizeof(lock_t)));
	pthread_mutex_init(&m->m, NULL);
	return m;
}

int lock_set(lock_t *m, int rank)
{
	pthread_mutex_lock(&m->m);
	return 0;
}

int lock_unset(lock_t *m, int rank)
{
	pthread_mutex_unlock(&m->m);
	return 0;
}


