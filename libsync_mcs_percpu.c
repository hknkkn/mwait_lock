#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <linux/unistd.h>
#include <stdio.h>
#include <sched.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "libsync.h"
#include "cycle.h"
#include <assert.h>
#include <sys/ioctl.h>
#include "util.h"
#include "cmpxchg.h"
#include "wait.h"
#include "log.h"

/*
MCS lock
http://www.cs.rochester.edu/research/synchronization/pseudocode/ss.html#K42
*/
struct lock {
	u16 next, tail;
};


#define EMPTY ((u16)-1)
#define HEAD ((u16)-2)
union mon {
	char pad[4096];
	struct {
		monitor_t locked;
		u16 next __attribute__((aligned(64)));
	};
} monitors[64] __attribute__((aligned(4096))) = { [0 ... 63] = {.next = EMPTY, .locked = 0}};

lock_t* lock_init_shared(void)
{
	lock_t *m;
	assert(0 == posix_memalign((void**)&m, 64, sizeof(lock_t)));
	m->next = m->tail = EMPTY;
	return m;
}

extern struct log logs[64];

#define l(_id, _head, _tail)
#define _l(_id, _head, _tail) do{ \
        struct log_item i = {.id = _id, .ts = getticks(), .head = _head, .tail = _tail}; \
        logs[rank].items[logs[rank].no++] = i; \
} while(0)


int lock_set(lock_t *l, int rank)
{
	u16 prev, next;
	int ret = 0;

	for (;;) {
		prev = ACCESS_ONCE(l->tail);
		l(1, prev, 0);
		if (prev == EMPTY) {
			if (cmpxchg(&l->tail, EMPTY, HEAD) == EMPTY) {
				l(2, 0, 0);
				return ret;
			}
		} else {
			if (cmpxchg(&l->tail, prev, rank) == prev) {
				l(3, prev, 0);
				monitors[rank].locked = 1;
				if (prev == HEAD)
					l->next = rank;
				else
					monitors[prev].next = rank;

				barrier();
//				__sync_synchronize();
				for (;ACCESS_ONCE(monitors[rank].locked);) {
					__monitor(&monitors[rank].locked, 0, 0);
					if (ACCESS_ONCE(monitors[rank].locked) == 0)
						break;
					ret++;
					__mwait(0, 0);
				}
				l(4, 0, 0);
				next = ACCESS_ONCE(monitors[rank].next);
				if (next == EMPTY) {
					l->next = EMPTY;
					if (cmpxchg(&l->tail, rank, HEAD) != rank) {
						l(5, 0, 0);
						for (;(next = ACCESS_ONCE(monitors[rank].next)) == EMPTY;) {
							__monitor(&monitors[rank].next, 0, 0);
							if ((next = ACCESS_ONCE(monitors[rank].next)) != EMPTY)
								break;
							ret++;
							__mwait(0, 0);
						}
						l->next = ACCESS_ONCE(monitors[rank].next);
					}
				} else {
					l->next = next;
					l(6, next, 0);
				}
				monitors[rank].next = EMPTY;
				return ret;
			}
		}
	}
}

int lock_unset(lock_t *l, int rank)
{
	u16 next = ACCESS_ONCE(l->next);
	int ret = 0;

	if (next == EMPTY) {
		if (cmpxchg(&l->tail, HEAD, EMPTY) == HEAD) {
			l(7, 0, 0);
			return ret;
		}
		l(8, 0, 0);
		for(;ACCESS_ONCE(l->next) == EMPTY;)
			cpu_relax();
		next = ACCESS_ONCE(l->next);
	}
	l(9, next, 0);
	monitors[next].locked = 0;
//	__sync_synchronize();
//	barrier();
	return ret;
}


