#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <linux/unistd.h>
#include <stdio.h>
#include <sched.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "libsync.h"
#include "cycle.h"
#include <assert.h>
#include <sys/ioctl.h>
#include "util.h"
#include "cmpxchg.h"
#include "wait.h"

/*
MCS lock
http://www.cs.rochester.edu/research/synchronization/pseudocode/ss.html#K42
*/
struct lock {
	struct lock *next;
	union {
		monitor_t locked;
		struct lock *tail;
	};
};


lock_t* lock_init_shared(void)
{
	lock_t *m;
	assert(0 == posix_memalign((void**)&m, 64, sizeof(lock_t)));
	m->next = m->tail = NULL;
	return m;
}

int lock_set(lock_t *l, int rank)
{
	lock_t monitor = {NULL, {.locked = 1}};
	lock_t *prev, *next;
	int ret = 0;

	for (;;) {
		prev = ACCESS_ONCE(l->tail);
		if (prev == NULL) {
			if (cmpxchg(&l->tail, NULL, (lock_t*)&l->next) == NULL)
				return ret;
		} else {
			if (cmpxchg(&l->tail, prev, &monitor) == prev) {
				monitor.locked = 1;
				prev->next = &monitor;
				for (;ACCESS_ONCE(monitor.locked);)
					cpu_relax();
				// got the lock
				next = ACCESS_ONCE(monitor.next);
				if (next == NULL) {
					l->next = NULL;
					if (cmpxchg(&l->tail, &monitor, (lock_t*)&l->next) != &monitor) {
						for (;ACCESS_ONCE(monitor.next) == NULL;)
							cpu_relax();
						l->next = monitor.next;
					}
					return ret;
				} else {
					l->next = monitor.next;
					return ret;
				}
			}
		}
	}
}

int lock_unset(lock_t *l, int rank)
{
	lock_t *next = ACCESS_ONCE(l->next);
	int ret = 0;

	if (next == NULL) {
		lock_t *reset = (lock_t*)&l->next;
		if (cmpxchg(&l->tail, reset, NULL) == reset)
			return ret;
		for (;(next = ACCESS_ONCE(l->next)) == NULL;)
			cpu_relax();
	}
	next->locked = 0;
	barrier();
	return ret;
}


