#define _GNU_SOURCE 
#include <sys/types.h>
#include <sys/syscall.h>
#include <linux/unistd.h>
#include <stdio.h>
#include <sched.h>
#include <errno.h>
#include <omp.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include "libsync.h"
#include "cycle.h"
#include "util.h"
#include <assert.h>
#include <signal.h>
#include <math.h>
#include <limits.h>
#include "log.h"

//struct log logs[64]  = { [0 ... 63] = {.no = 0, .items = { [0 ... 999999] = {.id = -1}}}};
struct log logs[64]  = { [0 ... 63] = {.no = 0}};

static void mutex_test(int iter, int inner_spin, int outer_spin)
{
	lock_t* m = lock_init_shared();

	double min_time = 9e100;
	double max_time = 0;
	double avg_time = 0;
	double stddev_time = 0;

	double avg_acquire = 0;
	double stddev_acquire = 0;
	int min_acquire = INT_MAX;
	int max_acquire = 0;
	int total_acquire = 0;

	int num = 0;

	
	int n_locks = 250, i;
	lock_t **locks = (lock_t**)malloc(n_locks * sizeof(lock_t*));

	for (i = 0; i < n_locks; i++)
		locks[i] = lock_init_shared();
	// to prevent the compiler from optimizing out some stuff
	int result = 0;

	#pragma omp parallel \
		default(shared)
	{
		int z = iter; //20000000;
		int rank = omp_get_thread_num();
		int cpu = sched_getcpu();
		unsigned short xsubi[3] = {0xcf, rank, cpu};
		int out = 0;
		int out2 = 0;
		int acquire = 0;
		ticks tmp1, tmp2, took;
//		set_affinity(cpu);
	//	say_hi(rank, cpu);

		// warm up
		lock_set(m, rank);
		lock_unset(m, rank);
		__sync_synchronize();
		// The lock protects a critical region.
		// The critical operation that needs to be
		// protected here is modifications of z and i.
		// At the end, the program must output
		// i_total=X where X is equal to the initial
		// value of z.
		// The value of i is the number of times each
		// thread takes the lock. For a fair lock,
		// we expect this number to be very close in
		// each thread.
		tmp1 = getticks();
		while (z > 0) {
			lock_t *l = locks[rand() % n_locks];
			out += lock_set(l, rank);
			if (z-- > 0)
				acquire++;
			num++;
//			out2 += spin((nrand48(xsubi) & ((512 * 1) - 1)) , nrand48(xsubi));
			out2 += do_spin(inner_spin, nrand48(xsubi));
			lock_unset(l, rank);
			out2 += do_spin(outer_spin, nrand48(xsubi));
//			out2 += spin((nrand48(xsubi) & ((512 * 1) - 1)) , nrand48(xsubi));
		}
		tmp2 = getticks();
		took = elapsed(tmp2, tmp1);

// collect the stats
		#pragma omp critical
		{
			if (took > max_time) max_time = (double)took;
			if (took < min_time) min_time = (double)took;
			avg_time += (double)took;

			if (acquire > max_acquire) max_acquire = acquire;
			if (acquire < min_acquire) min_acquire = acquire;
			avg_acquire += acquire;
			total_acquire += acquire;

			result += out2;
		}
		#pragma omp barrier

		#pragma omp single
		{
			avg_time /= (double)omp_get_num_threads();
			avg_acquire /= (double)omp_get_num_threads();
		}
		#pragma omp barrier

		#pragma omp critical
		{
			stddev_time += pow((took - avg_time), 2);
			stddev_acquire += pow((acquire - avg_acquire), 2);
		}
		#pragma omp barrier

		#pragma omp single
		{
			stddev_time = sqrt(stddev_time / (double)omp_get_num_threads());
			stddev_acquire = sqrt(stddev_acquire / (double)omp_get_num_threads());
		}
		#pragma omp barrier
//		write_logs(rank);

	//	printf("Rank %2d done (%20d). Took %.5e spun=%10d z=%d\ti=%d\n", rank, out2, (double)took, out, z, acquire);
	}

/*	printf("total_acquire=%d result=%d\n"
		"cycles:{min=%.3e max=%.3e avg=%.3e stddev=%.3e (%%%.3f)}\n"
		"acquire:{min=%d max=%d avg=%d stddev=%d (%%%.3f)}\n", 
		total_acquire, result,
		min_time, max_time, avg_time, stddev_time, stddev_time * 100 / avg_time,
		min_acquire, max_acquire, (int)avg_acquire, (int)stddev_acquire, stddev_acquire * 100 / avg_acquire);
*/
	printf("%10d, %10d, %10d, "
		"%10.3e, %10.3e, %10.3e, %10.3e, "
		"%10d, %10d, %10d, %10d, ", 
		total_acquire, num, result,
		min_time, max_time, avg_time, stddev_time,
		min_acquire, max_acquire, (int)avg_acquire, (int)stddev_acquire);

}

// Use this to debug your lock. If you get deadlocks and you can't
// figure out why, compile the test with debugging symbols, run it,
// let it deadlock, attach to it with gdb, and call the this function
// (call dump()). 
void dump() {
	FILE *f = fopen("logs/dump.txt", "w+");
	dump_logs(f);
	fclose(f);
}

int  main(int argc, char *argv[])
{
	int iter = 2000000, 
		inner_spin = 0,
		outer_spin = 0;

	if (argc > 1)
		assert(1 == sscanf(argv[1], "%d", &iter));

	if (argc > 2)
		assert(1 == sscanf(argv[2], "%d", &inner_spin));

	if (argc > 3)
		assert(1 == sscanf(argv[3], "%d", &outer_spin));

	mutex_test(iter, inner_spin, outer_spin);
	return 0;
}
