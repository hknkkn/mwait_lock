#!/bin/bash


#for app in ./test_critical_region_*.exe; do
for app in ./test_barrier_*.exe; do
	name="${app//test_barrier_/}"
	name="${name//\.exe/}"
	dir="datab/$name"
	mkdir -p $dir

	for work_out in 1 7 127 511 1023 4095; do
		outfile=${dir}/${work_out}.dat
		rm -f $outfile
		echo -n doing $outfile
		for scale in 1 2 3 4 5 6 7 8 12 16 20 24 28 32 36 40 44 48 52 56 60 64; do
		#for scale in 1 2 4 64; do
			echo -n " $scale"
			echo -ne "$scale,\t" >> $outfile
			do_power omprun 1 $scale $app 100000 $work_out 0 >> $outfile
			sleep 0.5
		done
		echo
	done
done
