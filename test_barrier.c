#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/syscall.h>
#include <linux/unistd.h>
#include <stdio.h>
#include <sched.h>
#include <errno.h>
#include <omp.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include "libsync.h"
#include "cycle.h"
#include "util.h"
#include <assert.h>
#include <signal.h>
#include <math.h>
#include <limits.h>
#include "log.h"
#include <sys/time.h>

long long unsigned int calib(int n, int l) {
	struct timeval tv1, tv2;
	int out = 0;
	int try = 10000, i;
	long long unsigned int took = 0;
	int rank = omp_get_thread_num();
	int cpu = sched_getcpu();
	unsigned short xsubi[3] = {0xcf, rank, cpu};

	// see how much does it take to spin n times
	gettimeofday(&tv1, NULL);
	for (i = 0; i < try; i++) {
		out += do_spin((nrand48(xsubi) & n) | l, 100);
	}
	gettimeofday(&tv2, NULL);
	took = (tv2.tv_sec - tv1.tv_sec) * 1000000 + (tv2.tv_usec - tv1.tv_usec);

	printf("%d spins took %llu usecs. out=%d\n", n, took/try, out);
	return took/try;
}

static void barrier_test(int iter, int spinmask, int lowermask)
{
	int z = iter; //20000000;
	int num = 0;

	// to prevent the compiler from optimizing out some stuff
	int result = 0;

	#pragma omp parallel
	{
		int rank = omp_get_thread_num();
		int cpu = sched_getcpu();
		unsigned short xsubi[3] = {0xcf, rank, cpu};
		int out2 = 0;
		int acquire = 0;
		int iterval = iter;
		struct timeval tv1, tv2;
		long long unsigned int took;
		//ticks tmp1, tmp2, took;

		//tmp1 = getticks();
		gettimeofday(&tv1, NULL);
		while (iterval-- > 0) {
			out2 += do_spin((nrand48(xsubi) & spinmask) | lowermask, 100);
			#pragma omp barrier
		}
		//tmp2 = getticks();
		//took = elapsed(tmp2, tmp1);
		gettimeofday(&tv2, NULL);
		took = (tv2.tv_sec - tv1.tv_sec) * 1000000 + (tv2.tv_usec - tv1.tv_usec);

		if (rank == 0)
			printf("Rank %2d done (%20d). Took %.4f secs. itercount=%d\t\n", rank, out2, (double)took/1000000, iter);
	}
}

static void barrier_oh_test(int iter, int spinmask, int lowermask)
{
	int z = iter; //20000000;
	int num = 0;

	// to prevent the compiler from optimizing out some stuff
	int result = 0;

	long long unsigned int perspin = calib(spinmask, lowermask);
	#pragma omp parallel
	{
		int rank = omp_get_thread_num();
		int cpu = sched_getcpu();
		unsigned short xsubi[3] = {0xcf, rank, cpu};
		int out2 = 0;
		int acquire = 0;
		int iterval = iter;
		struct timeval tv1, tv2;
		long long unsigned int took;

		gettimeofday(&tv1, NULL);
		while (iterval-- > 0) {
			out2 += do_spin((nrand48(xsubi) & spinmask) | lowermask, 100);
			//printf("%d", out2);
			#pragma omp barrier
		}
		gettimeofday(&tv2, NULL);
		took = (tv2.tv_sec - tv1.tv_sec) * 1000000 + (tv2.tv_usec - tv1.tv_usec);

		if (rank == 0)
			printf("Rank %2d done (%20d). Took %.4f secs. itercount=%d\tper iter oh=%llu\n", rank, out2, (double)took/1000000, iter, (took/iter) - perspin);
	}
}

int  main(int argc, char *argv[])
{
	int iter = 2000000, 
		spin = 0,
		spin_min = 0,
		calib_spin;

	if (argc > 1)
		assert(1 == sscanf(argv[1], "%d", &iter));

	if (argc > 2)
		assert(1 == sscanf(argv[2], "%d", &spin));

	if (argc > 3)
		assert(1 == sscanf(argv[2], "%d", &spin_min));

	if (argc > 4) {
		assert(1 == sscanf(argv[3], "%d", &calib_spin));
		calib(calib_spin, 0);
	} else
		barrier_test(iter, spin, spin_min);

	return 0;
}
