#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <linux/unistd.h>
#include <stdio.h>
#include <sched.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "libsync.h"
#include "cycle.h"
#include <assert.h>
#include <sys/ioctl.h>
#include "util.h"
#include "cmpxchg.h"
#include "wait.h"

#define MAX_LOCKERS_MASK	0x3F
#define WADDR ((void *) 0xBADBEEFBAD)
#define CHECK_OWNER 0

typedef struct lock lock_t;

struct lock {
	volatile u32	ht;		// 16-bit head and tail
	volatile void*	waddr[MAX_LOCKERS_MASK+1];
#if CHECK_OWNER
	volatile u32	owner;		// for debugging, owner of the lock
#endif
};

lock_t* lock_init_shared(void)
{
	int i;
	lock_t *l;

	assert(0 == posix_memalign((void**)&l, 128, sizeof(lock_t)));
	memset(l, 0, sizeof(*l));
	for(i = 0; i < MAX_LOCKERS_MASK + 1; i++)
		l->waddr[i] = WADDR;
#if CHECK_OWNER
	l->owner = 0xFFFFFFFF;
#endif

	return l;
}

int lock_set(lock_t *l, int rank)
{
	u32 old, head, tail;
	volatile u32 wait;
	old = xadd_sync(&l->ht, 1<<16);
	head = old & 0xFFFF;
	tail = old >> 16;

//	printf("lock_set:%d %d:%d %p %08x\n", rank, head, tail, &wait, l->ht);
	if (head != tail) {
		wait = rank;
//		printf("lock_set:%d [%d] <- %p\n", rank, (tail-1) & MAX_LOCKERS_MASK, &wait);
		l->waddr[(tail-1) & MAX_LOCKERS_MASK] = &wait;
		barrier();
//		__sync_synchronize();
		sync_mwait_neq(&wait, rank);
	}

#if CHECK_OWNER
	old = cmpxchg(&l->owner, 0xFFFFFFFF, rank);
	if (old != 0xFFFFFFFF) {
		printf("lock_set:%d lock already owned by %d\n", rank, old);
	}
#endif

//	printf("lock_set:%d own the lock now\n", rank);
//	__sync_synchronize();
	barrier();
	return 0;
}

int lock_unset(lock_t *l, int rank)
{
	int i, n;
	volatile u32 *w;
	u32 old, head, tail;

#if CHECK_OWNER
	n = cmpxchg(&l->owner, rank, 0xFFFFFFFF);
	if (n != rank) {
		printf("lock_unset:%d lock owned by another process %d\n", rank, n);
	}
#endif

	old = xadd_sync(&l->ht, 1);
	head = old & 0xFFFF;
	tail = old >> 16;

//	printf("lock_unset:%d %d:%d\n", rank, head, tail);
	n = head & MAX_LOCKERS_MASK;

	if (head == 0xFFFF) {
		printf("lock_unset:%d overflow\n", rank);
		return -1;
	}

	if (head == 16383) {
		old++;	// we increased the head
//		printf("lock_unset:%d overflow prevention start old %08x\n", rank, old);
		while ((i = cmpxchg(&l->ht, old, old & (MAX_LOCKERS_MASK | (MAX_LOCKERS_MASK << 16)))) != old) {
//			printf("lock_unset:%d overflow prevention old %08x new %08x\n", rank, old, i);
			old = i;
		}
//		printf("lock_unset:%d overflow prevention done %08x %08x\n", rank, old, l->ht);
	}

	if (head+1 == tail) {
		// nobody is waiting to be awaken, we can just return
//		printf("lock_unset:%d nobody sleeping\n", rank);
		return 0;
	}

	w = l->waddr[n];
	if (w==WADDR) {
//		printf("lock_unset:%d wait for slot %d to become non-zero\n", rank, head);
		for(i = 0; i < 10000000; i++) {
			w = l->waddr[n];
			if (w != WADDR)
				break;
			barrier();
//			__sync_synchronize();
		}

		if (w == WADDR) {
			printf("lock_unset:%d WADDR waddr [%d] head %d tail %d\n", rank, n, head, tail);
			for(n = 0; n < MAX_LOCKERS_MASK+1; n++)
				printf("[%d] %p\n", n, l->waddr[n]);
			*w = -1;
			return -1;
		}
	}

//	printf("lock_unset:%d [%d]:%p wake up %d\n", rank, n, w, *w);
	l->waddr[n] = WADDR;
	*w = -1;
	barrier();
//	__sync_synchronize();

	return 0;
}
