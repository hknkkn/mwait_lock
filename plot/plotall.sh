#!/bin/bash

#run this in the data folder where each directory is an implementation


for work_in in 0 16 64 128; do
	for work_out in 0 16 64 128; do
		for y in 7 14; do

name=${work_in}x${work_out}
script="
set terminal png
name='$name'
field_y=${y}
if (field_y == 14) {
	fname=sprintf('../plot/png/%s_power_8.png', name)
} else {
	fname=sprintf('../plot/png/%s_time_8.png', name)
}

field_x=1
#field_y=3

set output fname
set title fname
set key Left left top reverse invert
set xrange [0:9]
"

delim=" "
script="$script
plot "
for impl in ticket_def_linux single_spinner pthread omp mcs_percpu mcs lucho; do
#for impl in *; do
	script="$script $delim '$impl/1000000/$name.dat' using field_x:field_y with lp title '$impl'"
	delim=","
done
script="$script ;"

echo "$script" > /tmp/plot.tmp

gnuplot /tmp/plot.tmp
#echo "$script"

		done
	done
done




#!/usr/bin/gnuplot


