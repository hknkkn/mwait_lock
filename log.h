
#include "cmpxchg.h"
#include "cycle.h"

struct log_item {
	char id;
	ticks ts;
	union {
		u32 head_tail;
		struct {
			u16 head, tail;
		};
	};
};

struct log {
	unsigned int no;
	struct log_item items[1000000];
};

extern struct log logs[64];

static inline void write_logs(int rank) {
	char fname[100];
	FILE *f;
	int i;

	sprintf(fname, "logs/log_%03d.txt", rank);
	f = fopen(fname, "w+");

	for (i = 0; i < logs[rank].no; i++) {
		fprintf(f, "%20llu\t%2d\t%2d\t%2d\n", 
			logs[rank].items[i].ts, 
			logs[rank].items[i].id, 
			logs[rank].items[i].head, 
			logs[rank].items[i].tail);
	}
	
	fclose(f);
}

static inline void dump_logs(FILE *f) {
	int i;
	int rank;

	for (rank = 0; rank < 64; rank++) {
		for (i = 0; i < logs[rank].no; i++) {
			fprintf(f, "%20llu\t%2d\t%2d\t%2d\t%2d\n", 
				logs[rank].items[i].ts,
				rank,
				logs[rank].items[i].id,
				logs[rank].items[i].head,
				logs[rank].items[i].tail);
		}
	}
}
