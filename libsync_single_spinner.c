#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <linux/unistd.h>
#include <stdio.h>
#include <sched.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "libsync.h"
#include "cycle.h"
#include <assert.h>
#include <sys/ioctl.h>
#include "util.h"
#include "cmpxchg.h"
#include "wait.h"
#include "log.h"

/*
MCS lock
http://www.cs.rochester.edu/research/synchronization/pseudocode/ss.html#K42
*/

struct lock {
	u16 head;
	union tail {
		u32 tail_val;
		struct {
			u16 cpu;
			u16 seq;
		};
	} tail;
};

#define EMPTY ((u16)-1)
#define HEAD ((u16)-2)
union percpu_wait {
	char pad[4096];
	u16 seq __attribute__((aligned(64)));
} monitors[64] __attribute__((aligned(4096))) = { [0 ... 63] = {.seq = 0}};

lock_t* lock_init_shared(void)
{
	lock_t *m;
	assert(0 == posix_memalign((void**)&m, 64, sizeof(lock_t)));
	m->head = m->tail.cpu = EMPTY;
	return m;
}

extern struct log logs[64];

#define l(_id, _head, _tail)
#define _l(_id, _head, _tail) do{ \
        struct log_item i = {.id = _id, .ts = getticks(), .head = _head, .tail = _tail}; \
        logs[rank].items[logs[rank].no++] = i; \
} while(0)

#define MASK ((1 << 32) - 1)
int lock_set(lock_t *l, int rank)
{
	int ret = 0;
	union tail me = {.cpu = rank, .seq = monitors[rank].seq};
	union tail prev;

	prev = xchg(&l->tail, me);
//	do {
//		prev.tail_val = l->tail.tail_val;
//	} while(prev.tail_val != cmpxchg(&l->tail.tail_val, prev.tail_val, me.tail_val));

	l(1, prev.cpu, prev.seq);

	if (prev.cpu != EMPTY && prev.cpu != rank) {
		for (;ACCESS_ONCE(monitors[prev.cpu].seq) == prev.seq;) {
			__monitor(&monitors[prev.cpu].seq, 0, 0);
			if (ACCESS_ONCE(monitors[prev.cpu].seq) != prev.seq)
				break;
			__mwait(0, 0);
		}

		l(3, prev.cpu, prev.seq);
		for (; ACCESS_ONCE(l->head) == prev.cpu;)
			cpu_relax();
	} 

	l->head = rank;
	barrier();
//	__sync_synchronize();
	monitors[rank].seq++;
	l(4, prev.cpu, monitors[rank].seq);
	return 0;
}

int lock_unset(lock_t *l, int rank)
{
//	union _lock newhead = {.cpu = rank, .seq = l->head.seq + 1};

	l(5, rank, 0);
	l->head = EMPTY;
//	l->seq++;
	barrier();
	//__sync_synchronize();
	return 0;
}


