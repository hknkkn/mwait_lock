#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <linux/unistd.h>
#include <stdio.h>
#include <sched.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "libsync.h"
#include "cycle.h"
#include <assert.h>
#include <sys/ioctl.h>
#include "util.h"
#include "cmpxchg.h"
#include "wait.h"

/*
This is our competition. It is an exact
duplicate of the code in the linux kernel.
It uses very same instructions.
*/

union lock_union {
	u32 head_tail;
	struct __raw_tickets {
		u16 head, tail;
	} tickets;
};

struct lock {
	union lock_union l;
};


lock_t* lock_init_shared(void)
{
	lock_t *m;
	assert(0 == posix_memalign((void**)&m, 64, sizeof(lock_t)));
	m->l.head_tail = 0;
	return m;
}

int lock_set(lock_t *m, int rank)
{
	register struct __raw_tickets inc = { .tail = 1 };
	int ret = 0;
	inc = xadd(&m->l.tickets, inc);


	for (;;) {
		if (inc.head == inc.tail)
			break;
		ret++;

		register s16 pos = (s16)(inc.tail - inc.head) + 1;
		while(pos--)
			cpu_relax();
		inc.head = ACCESS_ONCE(m->l.tickets.head);
	}

	barrier();
	return ret;
}

int lock_unset(lock_t *m, int rank)
{
	__add(&m->l.tickets.head, 1, "");
	return 0;
}


