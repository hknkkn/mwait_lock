
# This will compile each libsync_*.c file
# and link all test_*.c file with 

SHELL=/bin/bash

PRINT_PREFIX="  CC\t"

-include local.mk

CARGS+=-DCACHELINESIZE=$(CLSIZE) -fopenmp -lm -lnuma #-fdump-tree-ompexp

ifeq ($(D),1)
CARGS+=-g -O0
endif

ifeq ($(D),2)
CARGS+=-DDEBUG -g -O0
endif

ifeq ($(D),3)
CARGS+=-O2 -DDEBUG -g
endif

ifeq ($(D),4)
CARGS+=-O2 -g
endif

ifeq ($(D),)
CARGS+=-O3
endif

LIBSYNC_SRCS=$(wildcard libsync_*.c)
LIBSYNC_OBJS=$(LIBSYNC_SRCS:.c=.o)

GOMPDIRBASE=/home/hakan/proj/usrc-mwait/gcc-4.7.3/libgomp
GBB=$(GOMPDIRBASE)/build-
GOMPDIRS=$(wildcard $(GBB)*)
GOMPVARIANTS=$(patsubst $(GBB)%,%, $(GOMPDIRS))
BARRIER_TEST_EXES=$(patsubst %,test_barrier_%.exe, $(GOMPVARIANTS))


IMPL_OBJ=libsync_$(IMPL).o
TEST_SRCS=test_critical_region.c test_many_locks.c
TEST_EXES=$(patsubst test_%.c,test_%_$(IMPL).exe,$(TEST_SRCS))

all: $(LIBSYNC_OBJS) $(BARRIER_TEST_EXES)

libsync_%.o: libsync_%.c *.h $(TEST_SRCS) local.mk
	@echo -e ${PRINT_PREFIX} $@
	@gcc -fopenmp -c $< -o $@ $(CARGS)
	@make --no-print-directory IMPL=$* D=$(D) tests

tests: $(TEST_EXES)

test_%_$(IMPL).exe: test_%.c $(IMPL_OBJ) *.h local.mk
	@echo -e ${PRINT_PREFIX} $@
	@gcc $< $(OMP_LIBS) $(IMPL_OBJ) $(OMP_LIBS) -o $@ $(CARGS) $(OMP_LIBS)


test_barrier_%.exe: test_barrier.c  *.h $(GBB)%/.libs/libgomp.a
	@echo -e $(PRINT_PREFIX) $@
	@gcc -I$(GBB)$* --openmp -O3 $< -o $@ $(GBB)$*/.libs/libgomp.a -lm -lnuma

clean:
	rm -f *.o *.exe
